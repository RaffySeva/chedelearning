-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 07, 2018 at 07:16 PM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e_learning`
--

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `courseCode` varchar(11) NOT NULL,
  `programID` int(11) NOT NULL,
  `courseDesc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`courseCode`, `programID`, `courseDesc`) VALUES
('0', 1, 'Integrative Programming II'),
('123', 23, 'undefined');

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE `department` (
  `deptID` int(11) NOT NULL,
  `schoolID` int(11) NOT NULL,
  `chairID` int(11) NOT NULL,
  `dept_name` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`deptID`, `schoolID`, `chairID`, `dept_name`) VALUES
(0, 0, 0, 'DCIS');

-- --------------------------------------------------------

--
-- Table structure for table `discusstion`
--

CREATE TABLE `discusstion` (
  `discussionID` int(11) NOT NULL,
  `question` varchar(55) NOT NULL,
  `description` varchar(100) NOT NULL,
  `rating` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `facultyId` int(20) NOT NULL,
  `schoolId` int(255) NOT NULL,
  `departmentId` int(20) NOT NULL,
  `fName` varchar(33) NOT NULL,
  `lName` varchar(33) NOT NULL,
  `mName` varchar(33) NOT NULL,
  `emailAddress` varchar(33) NOT NULL,
  `contractStatus` varchar(33) NOT NULL,
  `specialization` int(11) NOT NULL,
  `degree` varchar(33) NOT NULL,
  `graduationDate` date NOT NULL,
  `graduatedSchool` varchar(255) NOT NULL,
  `gender` varchar(33) NOT NULL,
  `dateOfBirth` date NOT NULL,
  `facultyType` varchar(33) NOT NULL,
  `dateHired` date NOT NULL,
  `password` varchar(255) NOT NULL,
  `accessLevel` int(33) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`facultyId`, `schoolId`, `departmentId`, `fName`, `lName`, `mName`, `emailAddress`, `contractStatus`, `specialization`, `degree`, `graduationDate`, `graduatedSchool`, `gender`, `dateOfBirth`, `facultyType`, `dateHired`, `password`, `accessLevel`) VALUES
(0, 0, 0, 'Eldrin', 'Augusto', 'Taneo', 'eldrinjake@yahoo.com', 'Full-time', 0, 'Doctoral', '2018-07-10', 'University of San Carlos', 'Male', '2018-07-01', 'President', '2017-12-04', 'test', 0),
(1, 0, 0, 'Dhalsim', 'Delos Reyes', 'Palma', 'dhalsim@gmail.com', 'Part-time', 0, 'Masteral', '2018-09-05', 'Cebu Institute of Technology University', 'Male', '2018-09-04', 'Teacher', '2017-08-07', 'test', 3);

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE `file` (
  `file_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `filetype` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `forum_comment`
--

CREATE TABLE `forum_comment` (
  `commentID` int(11) NOT NULL,
  `facID` int(11) NOT NULL,
  `forumID` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `rating` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `isHelpful` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forum_comment`
--

INSERT INTO `forum_comment` (`commentID`, `facID`, `forumID`, `comment`, `rating`, `date_added`, `isHelpful`) VALUES
(25, 1, 24, 'Okay', 0, '2018-09-12 19:24:36', 0),
(26, 0, 26, 'asda', 0, '2018-09-12 19:25:15', 0),
(27, 0, 24, 'fhg hfghfhd', 0, '2018-09-12 19:26:01', 0),
(28, 0, 27, 'asdas dasd  bfbr ', 0, '2018-09-12 19:33:29', 0),
(29, 1, 26, 'okay', 1, '2018-09-12 19:46:32', 0),
(33, 1, 23, 'asdada', 0, '2018-09-12 20:40:39', 0),
(34, 1, 23, 'vdfvdfv', 0, '2018-09-12 20:42:07', 0),
(35, 0, 23, 'qweqweqrqw', 0, '2018-09-12 21:56:28', 0),
(36, 0, 23, 'efwfw vwq', 0, '2018-09-12 21:56:36', 0),
(39, 1, 26, 'sfdsfwf', 0, '2018-09-13 18:33:49', 0),
(40, 1, 30, 'dfgegwvqeqve', 2, '2018-09-13 19:30:08', 0),
(43, 1, 28, 'fqwfwqfwqf', 0, '2018-09-14 14:44:50', 0),
(44, 1, 28, 'bfdbdb', 1, '2018-09-14 14:44:55', 0),
(45, 1, 30, 'ey', 0, '2018-10-08 23:41:28', 0);

-- --------------------------------------------------------

--
-- Table structure for table `forum_discussion`
--

CREATE TABLE `forum_discussion` (
  `discussionId` int(11) NOT NULL,
  `facultyId` int(11) NOT NULL,
  `category` varchar(50) NOT NULL,
  `question` varchar(50) NOT NULL,
  `description` varchar(200) NOT NULL,
  `rating` int(11) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forum_discussion`
--

INSERT INTO `forum_discussion` (`discussionId`, `facultyId`, `category`, `question`, `description`, `rating`, `status`, `date_added`) VALUES
(23, 0, 'Science', 'Ngano ing ani ni cya? ', 'qwerty uiiopo lkkj gfdfs cvbn ,mn dsadasdqd', 0, 'Open', '2018-09-12 19:14:14'),
(24, 1, 'Technology', 'AAAAA bbbbb CCCCC', 'asdad vdfv  sdf sdvds', 0, 'Closed', '2018-09-12 19:18:13'),
(25, 1, 'Mathematics', 'ZZZZ xxxx CCCC', 'dasda dasdw csadqwd', 0, 'Closed', '2018-09-12 19:20:24'),
(26, 0, 'Agriculture and Fisheries', 'My question ', 'details goes here', 0, 'Open', '2018-09-12 19:25:07'),
(27, 0, 'Engineering', 'How to do this and that', 'asdasd f fewf wef fwedq q', 0, 'Closed', '2018-09-12 19:31:32'),
(28, 0, 'Science', 'DDDDD', 'dfdsfs  fgwegf w wefwefewf', 0, 'Open', '2018-09-12 22:03:06'),
(30, 1, 'Agriculture and Fisheries', 'dfgdgwgw', 'wcqwecqwceq', 2, 'Open', '2018-09-12 22:14:18'),
(31, 1, 'Technology', 'dadadsad', 'dadadasdzzdz', 0, 'Open', '2018-09-14 21:42:14');

-- --------------------------------------------------------

--
-- Table structure for table `heiadmin`
--

CREATE TABLE `heiadmin` (
  `heiID` int(20) NOT NULL,
  `fName` varchar(55) NOT NULL,
  `lName` varchar(55) NOT NULL,
  `email` varchar(55) NOT NULL,
  `contactNo` bigint(20) NOT NULL,
  `password` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `heiadmin`
--

INSERT INTO `heiadmin` (`heiID`, `fName`, `lName`, `email`, `contactNo`, `password`) VALUES
(0, 'Eldrin', 'Augusto', 'eldrinjake@yahoo.com', 932323232, 'Aye');

-- --------------------------------------------------------

--
-- Table structure for table `institution`
--

CREATE TABLE `institution` (
  `instID` int(11) NOT NULL,
  `directorID` int(11) NOT NULL,
  `address` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `region` varchar(20) NOT NULL,
  `campus` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institution`
--

INSERT INTO `institution` (`instID`, `directorID`, `address`, `name`, `region`, `campus`) VALUES
(0, 0, 'Cebu', 'Univeresity of San Carlos', 'Region 7', 'Talamban');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(255) NOT NULL,
  `share` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `program_id` int(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `name` varchar(55) NOT NULL,
  `degree` varchar(55) NOT NULL,
  `STEAM_category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`program_id`, `school_id`, `name`, `degree`, `STEAM_category`) VALUES
(29, 0, 'aj', 'Bachelor', 'Science'),
(30, 0, 'zdada', 'Bachelor', 'Science'),
(31, 0, 'dqeq', 'Bachelor', 'Science'),
(32, 0, 'yryr', 'Bachelor', 'Science'),
(33, 0, 'eqeq', 'Bachelor', 'Science'),
(34, 0, 'test', 'Bachelor', 'Technology'),
(35, 0, 'adaadzczcz', 'Bachelor', 'Science');

-- --------------------------------------------------------

--
-- Table structure for table `rate_comment`
--

CREATE TABLE `rate_comment` (
  `rate_id` int(11) NOT NULL,
  `commentID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate_comment`
--

INSERT INTO `rate_comment` (`rate_id`, `commentID`, `userID`) VALUES
(8, 29, 2),
(18, 42, 1),
(20, 44, 1),
(21, 40, 1);

-- --------------------------------------------------------

--
-- Table structure for table `rate_forum`
--

CREATE TABLE `rate_forum` (
  `rate_id` int(11) NOT NULL,
  `discussionID` int(11) NOT NULL,
  `userID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rate_forum`
--

INSERT INTO `rate_forum` (`rate_id`, `discussionID`, `userID`) VALUES
(3, 30, 1);

-- --------------------------------------------------------

--
-- Table structure for table `reference`
--

CREATE TABLE `reference` (
  `reference_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `reference` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `school`
--

CREATE TABLE `school` (
  `school_id` int(11) NOT NULL,
  `instID` int(11) NOT NULL,
  `deanId` int(11) NOT NULL,
  `school_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `school`
--

INSERT INTO `school` (`school_id`, `instID`, `deanId`, `school_name`) VALUES
(0, 0, 0, 'University of San Carlos');

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int(11) UNSIGNED NOT NULL,
  `data` text CHARACTER SET utf8mb4 COLLATE utf8mb4_bin
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sessions`
--

INSERT INTO `sessions` (`session_id`, `expires`, `data`) VALUES
('zYVWQrI2-BoSfkS7TINdWYbEzAug54HW', 1541700757, '{\"cookie\":{\"originalMaxAge\":null,\"expires\":null,\"httpOnly\":true,\"path\":\"/\"},\"userID\":null,\"userId\":1,\"deptId\":0}');

-- --------------------------------------------------------

--
-- Table structure for table `specialization`
--

CREATE TABLE `specialization` (
  `specializationId` int(11) NOT NULL,
  `facultyId` int(11) NOT NULL,
  `course_code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `specialization`
--

INSERT INTO `specialization` (`specializationId`, `facultyId`, `course_code`) VALUES
(0, 0, '0');

-- --------------------------------------------------------

--
-- Table structure for table `suggestedactivity`
--

CREATE TABLE `suggestedactivity` (
  `suggested_activity_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `activity_title` text NOT NULL,
  `activity_content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `thesis`
--

CREATE TABLE `thesis` (
  `thesisID` int(11) NOT NULL,
  `facID` int(11) NOT NULL,
  `dateDefended` date NOT NULL,
  `publishStatus` varchar(55) NOT NULL,
  `topicArea` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `video_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `video_title` text NOT NULL,
  `filename` text NOT NULL,
  `filetype` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`courseCode`),
  ADD KEY `programID` (`programID`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`deptID`),
  ADD KEY `schoolID` (`schoolID`),
  ADD KEY `chairID` (`chairID`);

--
-- Indexes for table `discusstion`
--
ALTER TABLE `discusstion`
  ADD PRIMARY KEY (`discussionID`);

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`facultyId`),
  ADD KEY `deptId` (`departmentId`),
  ADD KEY `specialization` (`specialization`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `forum_comment`
--
ALTER TABLE `forum_comment`
  ADD PRIMARY KEY (`commentID`),
  ADD KEY `facID` (`facID`),
  ADD KEY `forumID` (`forumID`);

--
-- Indexes for table `forum_discussion`
--
ALTER TABLE `forum_discussion`
  ADD PRIMARY KEY (`discussionId`);

--
-- Indexes for table `heiadmin`
--
ALTER TABLE `heiadmin`
  ADD PRIMARY KEY (`heiID`);

--
-- Indexes for table `institution`
--
ALTER TABLE `institution`
  ADD PRIMARY KEY (`instID`),
  ADD KEY `directorID` (`directorID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`),
  ADD KEY `faculty_id` (`faculty_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`program_id`);

--
-- Indexes for table `rate_comment`
--
ALTER TABLE `rate_comment`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `rate_forum`
--
ALTER TABLE `rate_forum`
  ADD PRIMARY KEY (`rate_id`);

--
-- Indexes for table `reference`
--
ALTER TABLE `reference`
  ADD PRIMARY KEY (`reference_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`school_id`),
  ADD KEY `instID` (`instID`),
  ADD KEY `deanId` (`deanId`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `specialization`
--
ALTER TABLE `specialization`
  ADD PRIMARY KEY (`specializationId`),
  ADD KEY `facID` (`facultyId`),
  ADD KEY `course_desc` (`course_code`);

--
-- Indexes for table `suggestedactivity`
--
ALTER TABLE `suggestedactivity`
  ADD PRIMARY KEY (`suggested_activity_id`),
  ADD KEY `post_id` (`post_id`);

--
-- Indexes for table `thesis`
--
ALTER TABLE `thesis`
  ADD PRIMARY KEY (`thesisID`),
  ADD KEY `facID` (`facID`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`video_id`),
  ADD KEY `post_id` (`post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `forum_comment`
--
ALTER TABLE `forum_comment`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT for table `forum_discussion`
--
ALTER TABLE `forum_discussion`
  MODIFY `discussionId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `program_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `rate_comment`
--
ALTER TABLE `rate_comment`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `rate_forum`
--
ALTER TABLE `rate_forum`
  MODIFY `rate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `reference`
--
ALTER TABLE `reference`
  MODIFY `reference_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `suggestedactivity`
--
ALTER TABLE `suggestedactivity`
  MODIFY `suggested_activity_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `department`
--
ALTER TABLE `department`
  ADD CONSTRAINT `department_ibfk_1` FOREIGN KEY (`chairID`) REFERENCES `faculty` (`facultyId`),
  ADD CONSTRAINT `department_ibfk_2` FOREIGN KEY (`schoolID`) REFERENCES `school` (`school_id`);

--
-- Constraints for table `faculty`
--
ALTER TABLE `faculty`
  ADD CONSTRAINT `faculty_ibfk_1` FOREIGN KEY (`departmentId`) REFERENCES `department` (`deptID`),
  ADD CONSTRAINT `faculty_ibfk_2` FOREIGN KEY (`specialization`) REFERENCES `specialization` (`specializationId`);

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE;

--
-- Constraints for table `institution`
--
ALTER TABLE `institution`
  ADD CONSTRAINT `institution_ibfk_1` FOREIGN KEY (`directorID`) REFERENCES `heiadmin` (`heiID`);

--
-- Constraints for table `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_ibfk_1` FOREIGN KEY (`faculty_id`) REFERENCES `faculty` (`facultyId`) ON DELETE CASCADE;

--
-- Constraints for table `reference`
--
ALTER TABLE `reference`
  ADD CONSTRAINT `reference_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE;

--
-- Constraints for table `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_ibfk_1` FOREIGN KEY (`instID`) REFERENCES `institution` (`instID`),
  ADD CONSTRAINT `school_ibfk_2` FOREIGN KEY (`deanId`) REFERENCES `faculty` (`facultyId`);

--
-- Constraints for table `specialization`
--
ALTER TABLE `specialization`
  ADD CONSTRAINT `specialization_ibfk_2` FOREIGN KEY (`course_code`) REFERENCES `course` (`courseCode`);

--
-- Constraints for table `suggestedactivity`
--
ALTER TABLE `suggestedactivity`
  ADD CONSTRAINT `suggestedactivity_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE;

--
-- Constraints for table `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `video_ibfk_1` FOREIGN KEY (`post_id`) REFERENCES `post` (`post_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
