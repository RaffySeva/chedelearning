const express = require('express');
const mysql = require('mysql');
const app = express();
const bodyParser = require('body-parser');
const expbhs = require('express-handlebars');
var http = require('http');
var async = require("async");
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var fileUpload = require('express-fileupload');
var nodemailer = require("nodemailer");
var cookieParser = require('cookie-parser');
var exphbs = require('express-handlebars');
var expressValidator = require('express-validator');
var flash = require('flash');
var passport = require('passport');
var LocalStrategy = require('passport-local');
var LocalStrategy = require('Strategy');
const multer = require('multer');
const path = require('path');

const fs = require('fs');


const db = mysql.createConnection({

    host : 'localhost',
    user : 'root',
    password : '',
    database : 'e_learning',
    multipleStatements: true
});

var sessionStore = new MySQLStore({
    host : 'localhost',
    user : 'root',
    password : '',
    database : 'e_learning',
    multipleStatements: true
});

const storage = multer.diskStorage({
    destination: './public/uploads/',
    filename: function(req, file, cb) {
        cb(null, (Date.now() + ' - ' + file.originalname).toLowerCase());
    }
})
// Init upload
const uploadAny = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        checkFileTypeInfo(file, cb);
    }
}).any();

const uploadVideo = multer({
    storage: storage,
    fileFilter: function(req, file, cb) {
        checkFileTypeVideo(file, cb);
    }
}).single('video');

// Check File Type
function checkFileTypeVideo(file, cb) {
    // Allowed ext
    const filetypes = /mp4|mov|wmv|flv|avi/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if(mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Videos only');
    }
}

function checkFileTypeInfo(file, cb) {
    // Allowed ext
    const filetypes = /jpeg|jpg|png|doc|docx|xls|xlsx|ppt|pptx|pdf|txt/;
    // Check ext
    const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
    // Check mime
    const mimetype = filetypes.test(file.mimetype);

    if(mimetype && extname) {
        return cb(null, true);
    } else {
        cb('Error: Videos are not allowed');
    }
}

app.listen('3000',()=>{
    console.log("Server started at port 3000");

});

app.use(session({
    key: '69Atu22GZTSyDGW4sf4mMJdJ42436gAs',
    secret: '3dCE84rey8R8pHKrVRedgyEjhrqGT5Hz',
    store: sessionStore,
    resave: false,
    saveUninitialized: false
}));
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.urlencoded({ extended: true })); 

var dateFormat = require('dateformat');
var now = new Date();

// Public folder
app.use(express.static(__dirname + '/public'));

db.connect((err)=>{
    if(err){
        throw err;
    }
    console.log("Mysql went right");
});

// Body Parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });  
app.set('view engine', 'ejs');

app.use('/js',express.static(__dirname+ '/node_modules/bootstrap/dist/js'));
app.use('/js',express.static(__dirname+ '/node_modules/tether/dist/js'));
app.use('/js',express.static(__dirname+ '/node_modules/jquery/dist'));
app.use('/css',express.static(__dirname+ '/node_modules/bootstrap/dist/css'));
app.use('/img',express.static(__dirname+ '/node_modules/bootstrap/dist/img'));
app.use('/font',express.static(__dirname+ '/node_modules/bootstrap/dist/font'));

/** 
app.post('/',function(req, res){

    var query = 'BEGIN;';
    query+='INSERT INTO `course`(courseCode,programID,courseDesc) values (123,23,"'+req.body.course_desc+'");';
   //query+= 'INSERT INTO `program`(programID,deptID,name,degree,subject) values (1,1,"BSCS","'+req.body.course_degree+'","'+req.body.course_prog+'");';
    query+='COMMIT;';

    db.query(query,function(err,result){
        if (err) throw err;
        console.log("1 record inserted");
       //res.redirect("http://localhost:3000/");
   
    });
});
**/
/*TestAddCourse
app.get('/',function(req, res){

    var query = 'Select course.courseDesc from course LEFT JOIN specialization ON course.courseCode=specialization.course_code LEFT JOIN faculty ON specialization.specID=faculty.specialization where faculty.facID = 0';

    db.query(query,function(err,result){

        if(!!err){
            console.log(err);
    
        }
        res.render('pages/index',{
            siteTitle: "Test",
            pageTitle: "E-Learning Management System",
           
            items:result
        });
        
});

});


*/

/******
 * Post Learning Material
 ******/

app.get('/LearningMaterials', (req, res) => {

    if (req.session.userId != null) {
        var sql = "SELECT * FROM post JOIN faculty ON faculty.facultyId = post.faculty_id";

        db.query(sql, (err, result) => {
            res.render('pages/LearningMaterials', {
                siteTitle: "Learning Material",
                pageTitle: "E-learning Management System",
                items: result
            })
        })

    } else {
        return res.redirect('/');
    }

})

app.get('/LearningMaterial/id=:id', (req, res) => {
    
    if (req.session.userId) {
        var sql = `SELECT post.post_id, post.title, post.description, faculty.fName, faculty.lName FROM post JOIN faculty ON faculty.facultyId = post.faculty_id WHERE post.post_id = ${req.params.id}`;
        db.query(sql, (err, result1) => {
            var sql2 = `SELECT suggestedactivity.suggested_activity_id, suggestedactivity.activity_title, suggestedactivity.activity_content FROM suggestedactivity WHERE suggestedactivity.post_id = ${req.params.id}`;
            db.query(sql2, (err, result2) => {
                var sql3 = `SELECT reference, reference_id FROM reference WHERE post_id = ${req.params.id}`;
                db.query(sql3, (err, result3) => {
                    var sql4 = `SELECT video_id, video_title, filename, filetype FROM video WHERE post_id = ${req.params.id}`;
                    db.query(sql4, (err, result4) => {
                        var sql5 = `SELECT filename, file_id FROM file WHERE post_id = ${req.params.id}`;
                        db.query(sql5, (err, result5) => {
                            res.render('pages/LearningMaterial', {
                                siteTitle: "Learning Material",
                                pageTitle: "E-Learning Management System",
                                info: result1,
                                activity: result2,
                                reference: result3,
                                video: result4,
                                file: result5
                            })   
                        })
                          
                    })
                          
                })
            })
        })
        
    } else {
        return res.redirect('/');
    }

})

app.get('/InsertMaterial', (req, res) => {

    res.render('pages/InsertMaterial', {
        siteTitle: "Insert Material",
        pageTitle: "E-learning Management System",
        files: false,
        input: false,
        wrongfile: false
    })

})

app.post('/PostLearningMaterial', (req, res) => {

    if (req.session.userId != null) {
        uploadAny(req, res, (err) => {
            var generalTitle = req.body.generalTitle;
            var generalDescription = req.body.generalDescription;
            var checkbox = req.body.checkbox;
            var activityTitle = req.body.activityTitle;
            var activityDescription = req.body.activityDescription;
            var reference = req.body.reference;

            if (err) {
                console.log('ni sulod ko sa err');
                res.render('pages/InsertMaterial', {
                    siteTitle: "Insert Material",
                    pageTitle: "E-learning Management System",
                    files: false,
                    input: false,
                    wrongfile: true
                })
            } else {
                if (generalTitle == '' || generalDescription == '' || activityTitle == '' || activityDescription == '' || reference == '') {
                    res.render('pages/InsertMaterial', {
                        siteTitle: "Insert Material",
                        pageTitle: "E-learning Management System",
                        files: false,
                        input: true,
                        wrongfile: false
                    })
                } else if (req.files.length == 0 || req.files === undefined) {
                    res.render('pages/InsertMaterial', {
                        siteTitle: "Insert Material",
                        pageTitle: "E-learning Management System",
                        files: true,
                        input: false,
                        wrongfile: false
                    })
                } else {
                    var num = 0;

                    var object = {
                        checkbox : checkbox ? num = 1 : false
                    }

                    var sql = "INSERT INTO post (post_id, faculty_id, title, description, share) VALUES (NULL, "+req.session.userId+", '"+generalTitle+"', '"+generalDescription+"', "+num+")";
                    db.query(sql, (err, result) => {});

                    var length = req.files.length;

                    if (length > 1) {
                        for (i = 0; i < length; i++) {
                            insertFile(req.files[i].filename, req.files[i].mimetype);
                        }
                    } else {
                        insertFile(req.files[0].filename, req.files[0].mimetype);
                    }

                    insertActivity(activityTitle, activityDescription);
                    insertReference(reference);

                    res.redirect('UploadVideo');
                }
            }

            
        })
    } else {
        return res.redirect('/');
    }

})

app.get('/UploadVideo', (req, res) => {

    res.render('pages/UploadVideo', {
        siteTitle: "Upload Video",
        pageTitle: "E-learning Material System",
        wrongfile: false,
        nofile: false,
        input: false
    })

})

app.post('/UploadVideo', (req, res) => {

    if (req.session.userId) {
        uploadVideo(req, res, (err) => {
            var videoTitle = req.body.videoTitle;

            if (err) {
                console.log('ni sulod ko sa err');
                res.render('pages/UploadVideo', {
                    siteTitle: "Upload Video",
                    pageTitle: "E-learning Material System",
                    wrongfile: true,
                    nofile: false,
                    input: false
                })
            } else {
                if (req.file == undefined) {
                    console.log('ni sulod ko sa no file');
                    res.render('pages/UploadVideo', {
                        siteTitle: "Upload Video",
                        pageTitle: "E-learning Material System",
                        wrongfile: false,
                        nofile: true,
                        input: false
                    })
                }

                if (videoTitle == '') {
                    res.render('pages/UploadVideo', {
                        siteTitle: "Upload Video",
                        pageTitle: "E-learning Material System",
                        wrongfile: false,
                        nofile: false,
                        input: true
                    })
                } else {
                    insertVideo(videoTitle, req.file.filename, req.file.mimetype);
                    res.redirect('LearningMaterials'); // redirect to another page but for now kay same page
                }
            }
        })
    } else {
        res.redirect('/');  
    }

})

app.get('/updatevideo/id=:id', (req, res) => {
    if (req.session.userId) {
        var sql = `SELECT * FROM video WHERE video_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updatevideo', {
                siteTitle: "Update Video",
                pageTitle: "E-learning Management System",
                result: result,
                err: false
            })    
        })
        
    } else {
        return res.redirect('/');
    }
})

app.post('/updatevideo/id=:id', urlencodedParser, (req, res) => {

    var videotitle = req.body.videoTitle;

    if (videotitle == '') {
        var sql = `SELECT * FROM video WHERE video_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updatevideo', {
                siteTitle: "Update Video",
                pageTitle: "E-learning Management System",
                result: result,
                err: true
            })    
        })
    } else {
        var updatetitle = `UPDATE video SET video_title = '`+videotitle+`' WHERE video_id = ${req.params.id}`;

        db.query(updatetitle, (err, result) => {
            var sql = `SELECT post_id FROM video WHERE video_id = ${req.params.id}`;
            db.query(sql, (err, result2) => {
                res.redirect('/LearningMaterial/id='+result2[0]['post_id']);
            })
        })
    }

})

app.get('/deletevideo/id=:id', (req, res) => {
    var sql = `SELECT post_id FROM video WHERE video_id = ${req.params.id}`;
    var deletefromfolder = `SELECT filename FROM video WHERE video_id = ${req.params.id}`;
    var reference = `DELETE FROM video WHERE video_id = ${req.params.id}`;

    db.query(sql, (err, result1) => {
        db.query(deletefromfolder, (err, result2) => {
            deleteFromFolder(result2[0]['filename']);
            db.query(reference, (err, result3) => {
                res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
            })
        })
        
    });
})

app.get('/addvideo/id=:id', (req, res) => {
    if (req.session.userId != null) {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addvideo', {
                siteTitle: "Add Video",
                pageTitle: "E-learning Management System",
                result: result,
                wrongfile: false,
                nofile: false,
                input: false
            })
        })
    } else {
        return res.redirect('/');
    }
})

app.post('/addvideo/id=:id', (req, res) => {

    function insertLocal(title, filename, filetype, id) {
        console.log(id);
        var sql = "INSERT INTO `video`(`video_id`, `post_id`, `video_title`, `filename`, `filetype`) VALUES (NULL, "+id+", '"+title+"', '"+filename+"', '"+filetype+"')"

        db.query(sql, (err, result) => {
            console.log(result);
        })
    }

    uploadVideo(req, res, (err) => {
        var videoTitle = req.body.videoTitle;

        if (err) {
            console.log('ni sulod ko sa err');
            var sql = `SELECT * FROM post WHERE post_id = ${req.params.id}`;

            db.query(sql, (err, result) => {
                res.render('pages/addvideo', {
                    siteTitle: "Upload Video",
                    pageTitle: "E-learning Material System",
                    wrongfile: true,
                    nofile: false,
                    input: false
                })
            })
            
        } else {
            if (req.file == undefined) {
                var sql = `SELECT * FROM post WHERE post_id = ${req.params.id}`;

                db.query(sql, (err, result) => {
                    res.render('pages/addvideo', {
                        siteTitle: "Upload Video",
                        pageTitle: "E-learning Material System",
                        wrongfile: false,
                        nofile: true,
                        input: false
                    })
                })
            }

            if (videoTitle == '') {
                var sql = `SELECT * FROM post WHERE post_id = ${req.params.id}`;

                db.query(sql, (err, result) => {
                    res.render('pages/addvideo', {
                        siteTitle: "Upload Video",
                        pageTitle: "E-learning Material System",
                        wrongfile: false,
                        nofile: false,
                        input: true
                    })
                })
            } else {
                var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

                db.query(sql, (err, result) => {
                    insertLocal(videoTitle, req.file.filename, req.file.mimetype, result[0]['post_id']);
                    res.redirect('/LearningMaterial/id='+result[0]['post_id']); // redirect to another page but for now kay same page
                })

               
            }
        }
    })
})

app.get('/deleteallvideo/id=:id', (req, res) => {
    var sql = `SELECT filename FROM video WHERE post_id = ${req.params.id}`;
    var sql2 = `DELETE FROM video WHERE post_id = ${req.params.id}`;

    db.query(sql, (err, result) => {
        if(result.length == 0 || result === undefined) {
            res.redirect('/LearningMaterial/id='+req.params.id);
        } else {
            for (var i = 0; i < result.length; i++) {
                deleteFromFolder(result[i]['filename']);
            }
        }
        db.query(sql2, (err, result2) => {
            res.redirect('/LearningMaterial/id='+req.params.id);
        })
    })
})

app.get('/updateactivity/id=:id', (req, res) => {

    if (req.session.userId) {
        var sql = `SELECT * FROM suggestedactivity WHERE suggested_activity_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updateactivity', {
                siteTitle: "Update Activity",
                pageTitle: "E-learning Management System",
                result: result,
                err: false
            })
        })
    } else {
        return res.redirect('/');
    }

})

app.post('/updateactivity/id=:id', urlencodedParser, (req, res) => {

    var activitytitle = req.body.activityTitle;
    var activitydescription = req.body.activityDescription;

    if (activitytitle == '' || activitydescription == '') {
        var sql = `SELECT * FROM suggestedactivity WHERE suggested_activity_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updateactivity', {
                siteTitle: "Update Activity",
                pageTitle: "E-learning Management System",
                result: result, 
                err: true
            })
        })         
    } else {
        var updateactivity = `UPDATE suggestedactivity SET activity_title = '`+activitytitle+`', activity_content = '`+activitydescription+`' WHERE suggested_activity_id = ${req.params.id}`;

        db.query(updateactivity, (err, result) => {
            console.log(result);
            var sql = `SELECT post_id FROM suggestedactivity WHERE suggested_activity_id = ${req.params.id}`;
            db.query(sql, (err, result1) => {
                res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
            })
        })    
    }
})

app.get('/deleteactivity/id=:id', (req, res) => {
    var sql = `SELECT post_id FROM suggestedactivity WHERE suggested_activity_id = ${req.params.id}`;
    var reference = `DELETE FROM suggestedactivity WHERE suggested_activity_id = ${req.params.id}`;

    db.query(sql, (err, result1) => {
        db.query(reference, (err, result2) => {
            res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
        })
    });
})

app.get('/addactivity/id=:id', (req, res) => {
    if (req.session.userId != null) {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addactivity', {
                siteTitle: "Add Activity",
                pageTitle: "E-learning Management System",
                result: result,
                err: false
            })    
        })
        
    } else {
        return res.redirect('/');
    }
})

app.post('/addactivity/id=:id', urlencodedParser, (req, res) => {

    console.log(req.params.id);

    var activitytitle = req.body.activityTitle;
    var activitydescription = req.body.activityDescription;

    if (activitytitle == '' || activitydescription == '') {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addactivity', {
                siteTitle: "Update Reference",
                pageTitle: "E-learning Management System",
                result: result,
                err: true
            })    
        })
    } else {
        var sql = "INSERT INTO `suggestedactivity`(`suggested_activity_id`, `post_id`, `activity_title`, `activity_content`) VALUES (NULL, "+req.params.id+", '"+activitytitle+"', '"+activitydescription+"')";

        db.query(sql, (err, result) => {
            res.redirect('/LearningMaterial/id='+req.params.id);
        })
    }

})

app.get('/deleteallactivity/id=:id', (req, res) => {
    var sql = `DELETE FROM suggestedactivity WHERE post_id = ${req.params.id}`;

    db.query(sql, (err, result) => {
        res.redirect('/LearningMaterial/id='+req.params.id);
    })
})

app.get('/deleteallfile/id=:id', (req, res) => {
    var sql = `SELECT filename FROM file WHERE post_id = ${req.params.id}`;
    var sql2 = `DELETE FROM file WHERE post_id = ${req.params.id}`;

    db.query(sql, (err, result) => {
        if(result.length == 0 || result === undefined) {
            res.redirect('/LearningMaterial/id='+req.params.id);
        } else {
            for (var i = 0; i < result.length; i++) {
                deleteFromFolder(result[i]['filename']);
            }
        }
        db.query(sql2, (err, result2) => {
            res.redirect('/LearningMaterial/id='+req.params.id);
        })
    })
})

app.get('/addreference/id=:id', (req, res) => {
    if (req.session.userId != null) {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addreference', {
                siteTitle: "Add Reference",
                pageTitle: "E-learning Management System",
                result: result,
                err: false
            })    
        })
        
    } else {
        return res.redirect('/');
    }
})

app.post('/addreference/id=:id', urlencodedParser, (req, res) => {

    console.log(req.params.id);

    var referencetitle = req.body.referenceTitle;

    if (referencetitle == '') {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addreference', {
                siteTitle: "Update Reference",
                pageTitle: "E-learning Management System",
                result: result,
                err: true
            })    
        })
    } else {
        var sql = "INSERT INTO `reference`(`reference_id`, `post_id`, `reference`) VALUES (NULL, "+req.params.id+", '"+referencetitle+"')";

        db.query(sql, (err, result) => {
            res.redirect('/LearningMaterial/id='+req.params.id);
        })
    }

})

app.get('/deleteallreference/id=:id', (req, res) => {
    var sql = `DELETE FROM reference WHERE post_id = ${req.params.id}`;

    db.query(sql, (err, result) => {
        res.redirect('/LearningMaterial/id='+req.params.id);
    })
})

app.get('/updatereference/id=:id', (req, res) => {

    if (req.session.userId != null) {
        var sql = `SELECT reference, reference_id FROM reference WHERE reference_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updatereference', {
                siteTitle: "Update Reference",
                pageTitle: "E-learning Management System",
                result: result,
                err: false
            })
        })
    } else {
        return res.redirect('/');
    }

})

app.post('/updatereference/id=:id', urlencodedParser, (req, res) => {

    var updatetitle = req.body.title;

    if (updatetitle == '') {
        var sql = `SELECT reference, reference_id FROM reference WHERE reference_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/updatereference', {
                siteTitle: "Update Reference",
                pageTitle: "E-learning Management System",
                result: result, 
                err: true
            })
        })
    } else {
        var updatereference = `UPDATE reference SET reference = '`+ updatetitle +`' WHERE reference_id = ${req.params.id}`;

        db.query(updatereference, (err, result) => {
            var sql = `SELECT post_id FROM reference WHERE reference_id = ${req.params.id}`;
            db.query(sql, (err, result1) => {
                res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
            })
        }) 
    }

})

app.get('/addfile/id=:id', (req, res) => {

    if (req.session.userId != null) {
        var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

        db.query(sql, (err, result) => {
            res.render('pages/addfile', {
                siteTitle: "Add File",
                pageTitle: "E-learning Management System",
                result: result,
                wrongfile: false,
                nofile: false
            })
        })
    } else {
        return res.redirect('/');
    }

})

app.post('/addfile/id=:id', (req, res) => {

    function insertLocal(filename, filetype, id) {
        var sql = "INSERT INTO file (file_id, post_id, filename, filetype) VALUES (NULL, "+id+", '"+filename+"', '"+filetype+"')";

        db.query(sql, (err, result) => {})
    }

    uploadAny(req, res, (err) => {
        if(err) {
            var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

            db.query(sql, (err, result) => {
                res.render('pages/addfile', {
                    siteTitle: "Add File",
                    pageTitle: "E-learning Management System",
                    result: result,
                    wrongfile: true,
                    nofile: false
                })
            })
        }

        if (req.files.length == 0 || req.files === undefined) {
            var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

            db.query(sql, (err, result) => {
                res.render('pages/addfile', {
                    siteTitle: "Add File",
                    pageTitle: "E-learning Management System",
                    result: result,
                    wrongfile: false,
                    nofile: true
                })
            })
        } else {
            var length = req.files.length;

            var sql = `SELECT post_id FROM post WHERE post_id = ${req.params.id}`;

            db.query(sql, (err, result) => {
                if (length > 1) {
                    for (i = 0; i < length; i++) {
                        insertLocal(req.files[i].filename, req.files[i].mimetype, result[0]['post_id']);
                    }
                } else {
                    insertLocal(req.files[0].filename, req.files[0].mimetype, result[0]['post_id']);
                }

                res.redirect('/LearningMaterial/id='+result[0]['post_id']);
            })
        }
    })
})

app.get('/deletefile/id=:id', (req, res) => {
    var sql = `SELECT post_id FROM file WHERE file_id = ${req.params.id}`;
    var deletefromfolder = `SELECT filename FROM file WHERE file_id = ${req.params.id}`;
    var reference = `DELETE FROM file WHERE file_id = ${req.params.id}`;

    db.query(sql, (err, result1) => {
        db.query(deletefromfolder, (err, result2) => {
            deleteFromFolder(result2[0]['filename']);
            db.query(reference, (err, result3) => {
                res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
            })
        })
        
    });
})

app.get('/deletereference/id=:id', (req, res) => {
    var sql = `SELECT post_id FROM reference WHERE reference_id = ${req.params.id}`;
    var reference = `DELETE FROM reference WHERE reference_id = ${req.params.id}`;

    db.query(sql, (err, result1) => {
        db.query(reference, (err, result2) => {
            res.redirect('/LearningMaterial/id='+result1[0]['post_id']);
        })
    });
})

app.get('/download/id=:id', (req, res) => {

    var sql = `SELECT filename FROM file WHERE file_id = ${req.params.id}`;

    db.query(sql, (err, result) => {
        res.download('./public/uploads/' + result[0]['filename']);
    }) 

})

/************
 * End of Post Learning Material
 ************/


app.get("/SignUp",function(req,res){
    
    res.render('pages/SignUptest',{
        siteTitle: "Test",
        pageTitle: "E-Learning Management System",
       
        
    });
    
    
});
/**
 * Index
 */
app.get('/',function(req, res){
 
    
    req.session.userId = null;
   
        res.render('pages/Login',{
            siteTitle: "Test",
            pageTitle: "E-Learning Management System",
           
            
        });
        
        
});


/**
 * Help Desk
 */

 app.get('/HelpDesk',function(req,res){
    res.render("pages/HelpDesk",{
        pageTitle: "Help Desk",
        siteTitle: "E-learning Management System"
    })
 });
 /**
 * FAQ
 */
app.get('/FAQ',function(req,res){
    res.render("pages/FAQ",{
        pageTitle: "FAQ",
        siteTitle: "E-learning Management System"
    })
 });

/*****
 * 
 * Login
 */

app.post('/login',function(req, res){

    var query = "SELECT * from faculty where emailAddress='"+req.body.email+"' AND password ='"+req.body.password+"'";
    
    db.query(query, function(err, result){
        if(err)
            throw err;
            if(result.length!=0){
                req.session.userId = result[0].facultyId;
                req.session.deptId = result[0].departmentId;
                
                    res.render('pages/Index',{
                        siteTitle: "Test",
                        pageTitle: "E-Learning Management System",
                
                });
                
               

        }else{
            res.render('pages/LoginFail',{
                siteTitle: "Login",
                pageTitle: "E-Learning Management System",
               
                
            });
        }
    });
});

/** 
app.post('/',function(req, res){

    var query = 'BEGIN;';
    query+='INSERT INTO `course`(courseCode,programID,courseDesc) values (123,23,"'+req.body.course_desc+'");';
    query+= 'INSERT INTO `program`(programID,deptID,name,degree,subject) values (1,1,"BSCS","'+req.body.course_degree+'","'+req.body.course_prog+'");';
    query+='COMMIT;';

    db.query(query,function(err,result){
        if (err) throw err;
        console.log("1 record inserted");
       //res.redirect("http://localhost:3000/");
   
    });
});
**/
app.post('/AddProgram',function(req, res){
    if(req.session.userID!=null){
    console.log(req.body);
    var query = 'Insert into program(school_id,name,degree,STEAM_category) VALUES("'+req.body.prog_school+'","'+req.body.prog_name+'","'+req.body.prog_deg+'","'+req.body.prog_cat+'")';
    
            db.query(query,function(err,result){
                
                if (err) throw err;                   
                console.log("1 record inserted");
                      res.redirect("http://localhost:3000/AddProgram/");
                        
                });
            }else{
               return res.redirect("/");
            }
    });

/**
 *
 * Login Page
 *  
 */
app.get('/Login',function(req,res){
    res.render('pages/Login.ejs',{
        siteTitle: "Test",
        pageTitle: "E-Learning Management System"
            
    })
});
app.get('/Profile',function(req,res){
    if(req.session.userID!=null){
    res.render('pages/Profile.ejs',{
        siteTitle: "Test",
        pageTitle: "E-Learning Management System"
            
    });
    }else{
        return res.redirect("/");
    }
});
/**
 * Profile Page
 */

 app.get('/Profile',function(req,res){
 
 });
//Add Program page

app.get('/AddProgram',function(req, res){
    if(req.session.userID!=null){
    var query = 'Select program.program_id,school.school_name, program.name, program.degree, STEAM_category from program LEFT JOIN school ON program.school_id=school.school_id ';
    db.query(query,function(err,result){
        res.render('pages/AddProgram.ejs',{
            siteTitle: "Test",
            pageTitle: "E-Learning Management System",
            items: result
        });
    });
}else{
    return res.redirect("/");
}
     
     
       
});
//Forum page





app.post('/event/edit/:event_id',function(req, res){
    if(req.session.userID!=null){
    console.log(req.body);
    var query = 'UPDATE program SET ';
    query+= 'name ="'+req.body.name+'",';
    query+= 'degree ="'+req.body.degree+'",';
    query+= 'STEAM_category ="'+req.body.category+'"';
    query+= 'WHERE program_id="'+req.body.id+'"';
    db.query(query,function(err,result){
       
        
        if(result.affectedRows){
            res.redirect("localhost:3000/AddProgram");  
            
        }else{
            throw err;
        }
     
                        
    });
}else{
    return res.redirect("/");
}
});
    //------delete

    app.get('/event/delete/:event_id',function(req, res){
        if(req.session.userID!=null){
        var query = 'DELETE from program ';
      
        query+= 'WHERE program_id="'+req.params.event_id+'"';
        db.query(query,function(err,result){
           
            if (err) throw err;                   
            console.log("1 record deleted");
                  res.redirect("http://localhost:3000/AddProgram/");
      
                            
        });
    }else{
        return res.redirect("/");
    }
    });
//INSTITUTIONS
app.get('/',function(req, res){
    req.session.userID = null;
        res.render('pages/index',{
            siteTitle: "Test",
            pageTitle: "E-Learning Management System"
        });
});





    //FORUMS
    app.get('/forum', function(req, res) {
        if(req.session.userId!=null){
        var page = req.query.page;
        var ord = req.query.ord;
        var filter = req.query.cat;
        var status = req.query.status;
        const max = 5;
        var query;
        if(req.query.page == null || req.query.page==1){
            page=1;
        }
        if(req.query.ord==null ){
            ord="forum_discussion.date_added DESC";
        }
        if(req.query.cat == null || req.query.cat== "All"){
            filter = "All";
            if(req.query.status == null || req.query.status == "Both"){
                status = "Both";
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }else{
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId WHERE status = '"+status+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }
        }else{
            if(req.query.status == null || req.query.status == "Both"){
                status = "Both";
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE category='"+req.query.cat+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }else{
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE category='"+req.query.cat+"' AND status = '"+status+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }
            
        }  
        var q2 = " SELECT COUNT(*) as count from forum_discussion";
            db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                db.query(q2, function(err, result2) {
                    if(!!err){
                        console.log(err);
                    }else{
                        var mpage = parseInt(result2[0].count) / parseInt(max);
                        parseInt(page);
                            res.render('pages/forum', {  
                                siteTitle: "Test", 
                                pageTitle: "E-Learning Management System",
                                items: result,
                                mpage: mpage,
                                status: status,
                                max:max,
                                ord:ord,
                                count: result2,
                                filter: filter,
                                page: page,
                                session: req.session.userId
                               });
                    
                    }
                    
                }); 
            
            }
            
        }); 
        
        }else{
            return res.redirect('/');
        }
    });
    app.get('/forums/search', function(req, res) {
        if(req.session.userId!=null){
        var page = req.query.page;
        var ord = req.query.ord;
        var filter = req.query.cat;
        var status = req.query.status;
        const max = 5;
        var query;
        if(req.query.page == null || req.query.page==1){
            page=1;
        }
        if(req.query.ord==null ){
            ord="forum_discussion.date_added DESC";
        }
        if(req.query.cat == null || req.query.cat== "All"){
            filter = "All";
            if(req.query.status == null || req.query.status == "Both"){
                status = "Both";
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId WHERE (forum_discussion.question LIKE '%"+req.query.keyword+"%' OR faculty.fName LIKE '%"+req.query.keyword+"%' OR faculty.lName LIKE '%"+req.query.keyword+"%') GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }else{
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId WHERE (forum_discussion.question LIKE '%"+req.query.keyword+"%' OR faculty.fName LIKE '%"+req.query.keyword+"%' OR faculty.lName LIKE '%"+req.query.keyword+"%') AND status = '"+status+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }
        }else{
            if(req.query.status == null || req.query.status == "Both"){
                status = "Both";
                 query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE (forum_discussion.question LIKE '%"+req.query.keyword+"%' OR faculty.fName LIKE '%"+req.query.keyword+"%' OR faculty.lName LIKE '%"+req.query.keyword+"%') AND category='"+req.query.cat+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }else{
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE (forum_discussion.question LIKE '%"+req.query.keyword+"%' OR faculty.fName LIKE '%"+req.query.keyword+"%' OR faculty.lName LIKE '%"+req.query.keyword+"%') AND category='"+req.query.cat+"'  AND status = '"+status+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
            }
        }  
        var q2 = " SELECT COUNT(*) as count from forum_discussion";
            db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                db.query(q2, function(err, result2) {
                    if(!!err){
                        console.log(err);
                    }else{
                        var mpage = parseInt(result2[0].count) / parseInt(max);
                        parseInt(page);
                            res.render('pages/forum', {  
                                siteTitle: "Test", 
                                pageTitle: "E-Learning Management System",
                                items: result,
                                mpage: mpage,
                                max:max,
                                ord:ord,
                                status: status,
                                count: result2,
                                filter: filter,
                                keyword:req.query.keyword,
                                page: page,
                                session: req.session.userId
                               });
                    
                    }
                    
                }); 
            
            }
            
        }); 
        
        }else{
            return res.redirect('/');
        }
    });
    app.post('/createforum',function(req, res){
        if(req.session.userId!=null){
        var query = 'BEGIN;';
        query+='INSERT INTO `forum_discussion`(discussionId, facultyID, category, question, description, rating, status, date_added) values (null, '+req.session.userId+', "'+req.body.category+'", "'+req.body.question+'",  "'+req.body.description+'", 0, "Open", NOW());';
        query+='COMMIT;';
        db.query(query,function(err,result){
            if (err) throw err;
            console.log("1 record inserted");
            return res.redirect('/forum');
        });
        }else{
            return res.redirect('/');
        }
    });
    app.get('/forum_details', function(req, res) {
        if(req.session.userId!=null){ 
        const max = 5;
        var helpful=req.query.help;
        var page = req.query.page;
        var query = " SELECT * from forum_discussion LEFT JOIN faculty ON faculty.facultyId = forum_discussion.facultyId WHERE forum_discussion.discussionId = "+req.query.id;
        if(req.query.ord==null ){
            req.query.ord="forum_comment.rating desc";
        }
        
        if(req.query.page == null || req.query.page==1){
            page = 1;
        }
       
        if(helpful==1){
            var query2 = " SELECT  *, count(rate_comment.userID) as rc, forum_comment.commentID as comID  from forum_comment LEFT JOIN faculty ON faculty.facultyId = forum_comment.facID left join rate_comment on rate_comment.commentID = forum_comment.commentID  WHERE forumID = "+req.query.id+" AND isHelpful = 1 group by forum_comment.commentID  ORDER BY "+req.query.ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
        }else{
            helpful=0;
            var query2 = " SELECT  *, count(rate_comment.userID) as rc, forum_comment.commentID as comID  from forum_comment LEFT JOIN faculty ON faculty.facultyId = forum_comment.facID left join rate_comment on rate_comment.commentID = forum_comment.commentID  WHERE forumID = "+req.query.id+" group by forum_comment.commentID  ORDER BY "+req.query.ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
        }
        var query3 = " SELECT COUNT(forum_comment.comment) as 'cnt' from forum_comment WHERE forumID = "+req.query.id;
        var query4 = " SELECT *, COUNT(*) as 'rate' from rate_forum WHERE discussionId = "+req.query.id;
        var query5 = " SELECT COUNT(*) as 'check' from rate_forum WHERE userID = "+req.session.userId+" AND discussionId = "+req.query.id;
        var q6 = "SELECT *, rate_comment.commentID as comm from rate_comment join forum_comment on forum_comment.commentID = rate_comment.commentID join forum_discussion on forum_comment.forumID = forum_discussion.discussionId  GROUP BY rate_comment.rate_id";
        db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                db.query(query2, function(err, result2) {
                    if(!!err){
                        console.log(err);
                    }else{
                        db.query(query3, function(err, result3) {
                                if(!!err){
                                    console.log(err);
                                }
                                db.query(query4, function(err, result4) {
                                    if(!!err){
                                        console.log(err);
                                    }
                                    db.query(query5, function(err, result5) {
                                        if(!!err){
                                            console.log(err);
                                        }
                                        db.query(q6, function(err, result6) {
                                            if(!!err){
                                                console.log(err);
                                            }
                                            var mpage = parseInt(result3[0].cnt) / parseInt(max);
                                                res.render('pages/Forumdetails', {  
                                                    siteTitle: "Test", 
                                                  pageTitle: "E-Learning Management System",
                                                     items: result,
                                                     items2: result2,
                                                     items3: result3,
                                                     items4: result4,
                                                     items5: result5,
                                                     items6: result6,
                                                     mpage: mpage,
                                                     max:max,
                                                     page:page,
                                                     helpful: helpful,
                                                     sort: req.query.ord,
                                                     session: req.session.userId
                                                });                
                                           
                                                           
                                       
                                         });                
                                       
                                                       
                                   
                                     });         
                            });
                        
                         });           
      
                    }
                });
            }
            
        }); 
        }else{
            return res.redirect('/');
        }
    });
    app.get('/your_forum', function(req, res) {
        if(req.session.userId!=null){
            var page = req.query.page;
            var ord = req.query.ord;
            var filter = req.query.cat;
            var status = req.query.status;
            const max = 5;
            var query;
            if(req.query.page == null || req.query.page==1){
                page=1;
            }
            if(req.query.ord==null ){
                ord="forum_discussion.date_added DESC";
            }
            if(req.query.cat == null || req.query.cat== "All"){
                filter = "All";
                if(req.query.status == null || req.query.status == "Both"){
                    status = "Both";
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId WHERE faculty.facultyId="+req.session.userId+" GROUP BY forum_discussion.discussionId  ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
                }else{
                    query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate  from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId WHERE faculty.facultyId="+req.session.userId+" AND status = '"+status+"' GROUP BY forum_discussion.discussionId  ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
                }
            }else{
                if(req.query.status == null || req.query.status == "Both"){
                    status = "Both";
                query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE category='"+req.query.cat+"' AND faculty.facultyId="+req.session.userId+" GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
                }else{
                    query = " SELECT *, COUNT(commentID) as comcount, forum_discussion.date_added as date, forum_discussion.rating as rate from forum_discussion JOIN faculty ON faculty.facultyId = forum_discussion.facultyId LEFT join forum_comment ON forum_comment.forumID = forum_discussion.discussionId  WHERE category='"+req.query.cat+"' AND faculty.facultyId="+req.session.userId+" AND status = '"+status+"' GROUP BY forum_discussion.discussionId ORDER BY "+ord+" LIMIT "+ (parseInt(max) * parseInt(page) - parseInt(max))  +", "+max;
                }
    
            }  
            var q2 = " SELECT COUNT(*) as count from forum_discussion";
                db.query(query, function(err, result) {
                if(!!err){
                    console.log(err);
                }else{
                    db.query(q2, function(err, result2) {
                        if(!!err){
                            console.log(err);
                        }else{
                            var mpage = parseInt(result2[0].count) / parseInt(max);
                            parseInt(page);
                                res.render('pages/yourforum', {  
                                    siteTitle: "Test", 
                                    pageTitle: "E-Learning Management System",
                                    items: result,
                                    mpage: mpage,
                                    max:max,
                                    ord:ord,
                                    status: status,
                                    count: result2,
                                    filter: filter,
                                    page: page,
                                    session: req.session.userId
                                   });
                        
                        }
                        
                    }); 
                
                }
                
            }); 
            
            }else{
                return res.redirect('/');
            }
    });
    app.get('/deleteforum',function(req, res){
        if(req.session.userId!=null){
        var id = req.query.id;
        var query = 'BEGIN;';
        query+='DELETE from forum_discussion WHERE discussionId='+id+';';
        query+='COMMIT;';
        db.query(query,function(err,result){
            if (err) throw err;
            console.log("1 record deleted");
            return res.redirect('/your_forum');
        });
        }else{
            return res.redirect('/');
        }
    });
    app.get('/edit_forum', function(req, res) {
        if(req.session.userId!=null){ 
        var query = " SELECT * from forum_discussion WHERE discussionId = "+req.query.id;
        db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                    res.render('pages/editforum', {  
                        siteTitle: "Test", 
                        pageTitle: "E-Learning Management System",
                         items: result
                     });
            }
            
        }); 
        }else{
            return res.redirect('/');
        }
    });
    app.get('/markclosed', function(req, res) {
        if(req.session.userId!=null){
        var query = " update forum_discussion SET forum_discussion.status='Closed' WHERE forum_discussion.discussionId = "+req.query.id;
        db.query(query,function(err,result){
            if (err) throw err;
            console.log("1 record updated");
            return res.redirect('/forum_details?id='+req.query.id);
        });
        }else{
            return res.redirect('/');
        }
    });
    app.post('/updateforum', function(req, res) {
        if(req.session.userId!=null){
        var query = " update forum_discussion SET category='"+req.body.category+"',question='"+req.body.question+"', description='"+req.body.description+"' WHERE discussionId = "+req.body.id+"  ";
        db.query(query,function(err,result){
            if (err) throw err;
            console.log("1 record updated");
            return res.redirect('/forum_details?id='+req.body.id);
        });
        }else{
            return res.redirect('/');
        }
    });
    app.get('/rateforum', function(req, res) {
        if(req.session.userId!=null){ 
            var query ='INSERT INTO rate_forum(rate_id, discussionId, userID) values (null, "'+req.query.id+'", '+req.session.userId+' );';
            var query2 ='update forum_discussion SET rating=rating+1 WHERE discussionId='+req.query.id;
        db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                db.query(query2, function(err2, result2) {
                    if(!!err2){
                        console.log(err2);
                    }else{
            
                        return res.redirect('/forum_details?id='+req.query.id+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
                    }
                    
                }); 
            }
            
        }); 
        }else{
            return res.redirect('/');
        }
    });
    app.get('/unrateforum',function(req, res){
        if(req.session.userId!=null){
        var id = req.query.id;
        var query = 'BEGIN;';
        query+='DELETE FROM rate_forum WHERE discussionId='+id+' AND userID ='+req.session.userId+';';
        query+='COMMIT;';
        var query2 ='update forum_discussion SET rating=rating-1 WHERE discussionId='+req.query.id;
            db.query(query,function(err,result){
                if (err) throw err;
                db.query(query2,function(err,result2){
                    if (err) throw err;
                    return res.redirect('/forum_details?id='+id+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
                });
            });
        }else{
            return res.redirect('/');
        }
    });
    //END OF FORUM
    
    
    //COMMENT
    app.post('/comment',function(req, res){
        var query = 'BEGIN;';
        query+='INSERT INTO `forum_comment`(commentID, facID, forumID, comment, rating, date_added, isHelpful) values (null, '+req.session.userId+', "'+req.body.id+'",  "'+req.body.comment+'", 0, NOW(), 0);';
        query+='COMMIT;';
        db.query(query,function(err,result){
            if (err) throw err;
            console.log("1 record inserted");
            return res.redirect('/forum_details?id='+req.body.id);
        });
    });
    app.get('/delete_comment',function(req, res){
        if(req.session.userId!=null){
        var id = req.query.id;
        var query2='SELECT * from forum_discussion join forum_comment on forum_comment.forumID = discussionId WHERE forum_comment.facID='+req.session.userId+' AND forum_comment.commentID='+id;
        var query = 'BEGIN;';
        query+='DELETE from forum_comment WHERE commentID ='+id+';';
        query+='COMMIT;';
        db.query(query2,function(err,result2){
            if (err) throw err;
            console.log("1 record deleted");
            db.query(query,function(err,result){
                if (err) throw err;
                return res.redirect('/forum_details?id='+result2[0].discussionId);
            });
        });
        }else{
            return res.redirect('/');
        }
    });
    app.get('/markhelpful', function(req, res) {
        if(req.session.userId!=null){ 
             var query ='update forum_comment SET isHelpful=1 WHERE commentID='+req.query.id;
             db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                 return res.redirect('/forum_details?id='+req.query.did+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
             }
                                
            }); 
        }else{
            return res.redirect('/');
        }
    });
    app.get('/unmarkhelpful', function(req, res) {
        if(req.session.userId!=null){ 
             var query ='update forum_comment SET isHelpful=0 WHERE commentID='+req.query.id;
             db.query(query, function(err, result) {
            if(!!err){
                console.log(err);
            }else{
                 return res.redirect('/forum_details?id='+req.query.did+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
             }
                                
            }); 
        }else{
            return res.redirect('/');
        }
    });
    app.get('/ratecomment', function(req, res) {
        if(req.session.userId!=null){ 
            var israte;
            var check = "SELECT * from rate_comment where userID="+req.session.userId+" and commentID="+req.query.id;
            db.query(check, function(errc, resultc) {
                if(!!errc){
                    console.log(errc);
                }else{
                    if(resultc.length==0){
                        var query ='INSERT INTO rate_comment(rate_id, commentID, userID) values (null, "'+req.query.id+'", '+req.session.userId+' );';
                        var query2 ='update forum_comment SET rating=rating+1 WHERE commentID='+req.query.id;
                         db.query(query, function(err, result) {
                        if(!!err){
                            console.log(err);
                        }else{
                            db.query(query2, function(err2, result2) {
                                if(!!err2){
                                    console.log(err2);
                                }else{
                                    israte = encodeURIComponent('yes');
                                    return res.redirect('/forum_details?id='+req.query.did+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
                                }
                                
                            }); 
                        }
                        
                    }); 
                    }else{
                        var query = 'DELETE FROM rate_comment WHERE userID = '+req.session.userId+' AND commentID='+req.query.id;
                        var query2 ='update forum_comment SET rating=rating-1 WHERE commentID='+req.query.id;
                         db.query(query, function(err, result) {
                        if(!!err){
                            console.log(err);
                        }else{
                            db.query(query2, function(err2, result2) {
                                if(!!err2){
                                    console.log(err2);
                                }else{
                                    israte = encodeURIComponent('no');;
                                    return res.redirect('/forum_details?id='+req.query.did+'&help='+req.query.help+'&page='+req.query.page+'&ord='+req.query.ord);
                                }
                                
                            }); 
                        }
                        
                        }); 
                    }
                }  
            }); 
        }else{
            return res.redirect('/');
        }
    });
    //END comment
    
    
    
    
//END comment





function insertVideo(title, filename, filetype) {
    var sql = "SELECT post_id FROM post ORDER BY post_id DESC LIMIT 1";
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result[0].post_id);
        var insert = "INSERT INTO video (video_id, post_id, video_title, filename, filetype) VALUES (NULL, "+result[0].post_id+", '"+title+"', '"+filename+"', '"+filetype+"')";
        db.query(insert, (err, result) => {})
    })
}

function insertFile(filename, filetype) {
    var sql = "SELECT post_id FROM post ORDER BY post_id DESC LIMIT 1";
    db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result[0].post_id);
        var insert = "INSERT INTO file (file_id, post_id, filename, filetype) VALUES (NULL, "+result[0].post_id+", '"+filename+"', '"+filetype+"')";
        db.query(insert, (err, result) => {})
    })
}

function insertActivity(title, content) {
    var sql = "SELECT post_id FROM post ORDER BY post_id DESC LIMIT 1";
    db.query(sql, (err, result) => {
        if (err) throw err;
        var insert = "INSERT INTO suggestedactivity (suggested_activity_id, post_id, activity_title, activity_content) VALUES (NULL, "+result[0].post_id+", '"+title+"', '"+content+"')";
        db.query(insert, (err,result) => {})
    })
}

function insertReference(reference) {
    var sql = "SELECT post_id FROM post ORDER BY post_id DESC LIMIT 1";
    db.query(sql, (err, result) => {
        if (err) throw err;
        var insert = "INSERT INTO reference (reference_id, post_id, reference) VALUES (NULL, "+result[0].post_id+", '"+reference+"')";
        db.query(insert, (err, result) => {})
    })
}

function deleteFromFolder(filename) {
    console.log(filename);
    fs.unlink('./public/uploads/'+filename, (err) => {
        if (err) throw err;
    });
}

//Nodemailer
app.post('/SendEmail',function(req,res){
    const output = `<p>Name: ${req.body.email_name}</p>
                    <p>Email Address: ${req.body.email_address}</p>
                    <h3>Message</h3>
                    <p>${req.body.email_message}</p>`;

let transporter = nodemailer.createTransport({
    service: 'gmail',
    port: 25,
    secure: false, // true for 465, false for other ports
    auth: {
        user: "willzboncales@gmail.com", // generated ethereal user
        pass: "boncaleswi15104350_01201999" // generated ethereal password
    },
    tls:{
        rejectUnauthorized: false
    }
});

// setup email data with unicode symbols
let mailOptions = {
    from: '"CHED-eLearning" <willzboncales@gmail.com>', // sender address
    to: 'eldrinjake@yahoo.com', // list of receivers
    subject: 'Support Ticket ✔', // Subject line
    text: 'Gwapo ko', // plain text body
    html: output // html body
};

// send mail with defined transport object
transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
        return console.log(error);
    }
    console.log('Message sent: %s', info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
 
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
});
});
